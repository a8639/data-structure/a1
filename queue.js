let collection = [];

// Write the queue functions below.

function addToStack(element) {
    collection.push(element)

    addToStack("John")
    console.log(`The value has been enqueued. RESULT = ${collection}`)
    addToStack("Jane")
    console.log(`The value has been enqueued. RESULT = ${collection}`)

    

    return collection;
}


function removeFromStack() {
    collection.shift()
    console.log(`The value has been dequeued. RESULT = ${collection}`)
    return collection
}


function addElement(element) {
    collection.push(element)

    addElement("Bob")
    console.log(`The value has been enqueued. RESULT = ${collection}`)
    return collection
}

function peek() {
//   collection.peek()
  
  console.log(`The first value has been retrieved. RESULT = ${collection.peek()}`)

}



function getLength() {
    console.log(`The size of the queue has been retrieved. RESULT = ${collection.length}`)
}



function isEmpty() {
    if (collection.length === 0) {
        console.log (`The result has been retrieved. RESULT = false`)
    } else {
        console.log (`Queue is not empty`)
    }
}



function print() {
    //Print queue elements.
}


module.exports = {
    addToStack,
    removeFromStack,
    addElement,
    peek,
    getLength,
    isEmpty
};